#Crypto Currency Trader App
___
This app is DEMO Crypto Trader if you want to try how trading works but you don't want to use your real money yet.
##Prerequisites
___
JRE 8
MYSQL Database
##Database script

CREATE DATABASE cryptotrader;

CREATE TABLE `crypto` (
  `name` varchar(5) NOT NULL,
  `value` float NOT NULL
);
CREATE TABLE `wallett` (
  `name` varchar(5) NOT NULL,
  `quantity` float NOT NULL,
  `bought` float NOT NULL
);
INSERT INTO `wallett` (`name`, `quantity`, `bought`) VALUES
('USD', 10000, 0);

##How to use
If you want to view currencies values use:
localhost:8080/getCryptoAll
If you want to view your wallet use:
localhost:8080/getWallettAll
If you want to buy a currency use:
localhost:8080/buy?name=(here you write the crypto name)&amount=(amount you want to buy)
example: localhost:8080/buy?name =XRP&amount=1000
sell crypto:
localhost:8080/sell?name=(here you write the crypto name)&amount=(amount you want to sell)
example: localhost:8080/buy?name =XRP&amount=500
Update crypto values:
localhost:8080/update